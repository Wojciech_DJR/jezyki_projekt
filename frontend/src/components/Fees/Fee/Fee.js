import React, { Component } from 'react';
import axios from 'axios'
import '../../../styles/images/background/background.css'


class Fee extends Component {


    state = {
        deleted: false
    }
    markAsPayed = () => {
        console.log('markAsPayed')
        const config = {
            headers: {
                Authorization: "BEARER " + this.props.token
            }
        }
        const pushdata = {
            "pending": false,
            "payed": true
        }
        axios.put('http://localhost:8080/api/fees/' + this.props.id, pushdata, config).then(response => {
            if (response.status === 200) {
                console.log('powinno zmienic o tego')
                console.log(this.props.id)
                this.setState({ deleted: true })
                this.props.deleteNotPayed(); 


            }
        }
        )
    }


    render() {
        return (
            <div >
                {this.state.deleted === false ?
                    <div className="container ">
                        <table className="table table-responsive table-dark rounded text-left">
                            <tbody>
                                <tr>
                                    <th >Nr: {this.props.id}</th>
                                    <th> Treść rachunku: {this.props.reason}</th>
                                    <th>Kwota: {this.props.ammount} zł.</th>
                                </tr>
                            </tbody>
                        </table>
                        {this.props.pending === true ?
                                            <div className="center">
                                                <button className="btn btn-primary btn-lg btn-block" onClick={this.markAsPayed}>Opłaciłem rachunek nr {this.props.id}</button>
                                            </div> : null}       
                                            <hr className="bg-warning"></hr>                    
                    </div> 
                    : null}
               
            </div>

        );
    }
}
export default Fee

