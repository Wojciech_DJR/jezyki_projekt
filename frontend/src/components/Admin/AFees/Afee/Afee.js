import React, { Component } from 'react';
import axios from 'axios'



class Fee extends Component {


    state = {
        deleted: false
    }
    markAsPayed = () => {
        console.log('markAs')
        const config = {
            headers: {
                Authorization: "BEARER " + this.props.token
            }
        }
        const pushdata = {
            "payed": false,
            "verified": true
        }
        axios.put('http://localhost:8080/api/fees/' + this.props.id, pushdata, config).then(response => {
            if (response.status === 200) {
                console.log('powinno zmienic o tego')
                console.log(this.props.id)
                this.setState({ deleted: true })
                this.props.deletePending()


            }
        }
        )
    }


    render() {
        return (
            <div>
                {this.state.deleted === false ?
                    <div>
                        <table className="table table-responsive table-dark rounded text-left">
                            <tbody>
                            <tr>
                            <th>Nr: {this.props.id}</th>
                            <th>Treść rachunku: {this.props.reason}</th>
                            <th>Kwota: {this.props.ammount} zł.</th>
                    
                        </tr>
                        </tbody>
                        </table>
                        {this.props.payed === true ? 
                        <div className="center">
                            <button className="btn btn-primary btn-lg btn-block" onClick={this.markAsPayed}>Potwierdzony!</button></div> :
                            null}<hr className="bg-warning"></hr>  
                    </div> :null}
            </div>

        );
    }
}
export default Fee